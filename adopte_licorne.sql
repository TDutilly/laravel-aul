-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  ven. 03 juil. 2020 à 12:10
-- Version du serveur :  5.7.26
-- Version de PHP :  7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `adopte_licorne`
--

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Vente', '2020-06-29', '2020-06-29'),
(2, 'Reproduction', '2020-06-30', '2020-06-30');

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `unicorns`
--

DROP TABLE IF EXISTS `unicorns`;
CREATE TABLE IF NOT EXISTS `unicorns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `horn_size` float NOT NULL,
  `size` float NOT NULL,
  `color` varchar(9) COLLATE utf8_unicode_ci NOT NULL,
  `age` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `picture` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `prix` float DEFAULT NULL,
  `repro1_debut` date DEFAULT NULL,
  `repro1_fin` date DEFAULT NULL,
  `repro2_debut` date DEFAULT NULL,
  `repro2_fin` date DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  KEY `owner_id` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `unicorns`
--

INSERT INTO `unicorns` (`id`, `name`, `description`, `horn_size`, `size`, `color`, `age`, `category_id`, `picture`, `user_id`, `prix`, `repro1_debut`, `repro1_fin`, `repro2_debut`, `repro2_fin`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Marine Le Peigne', 'Une licorne fière de son pâturage, assez hostile envers les licornes qui ne lui ressemblent pas.', 0, 1.58, '#0011ff', 112, 2, 'https://www.brain-magazine.fr/m/posts/37411/originals/C-2aQpWXsAABEKC.jpg', 2, NULL, '2020-04-24', '2020-05-23', '2020-10-05', '2021-01-16', '2020-06-30', '2020-07-01', NULL),
(2, 'Adolphette', 'Une licorne autrichienne prédécoupée ', 60, 1.75, '#ff0000', 55, 1, 'https://thereviewmonster.files.wordpress.com/2019/11/hitler-eats-a-unicorn.jpg?w=605&h=335', 2, 3945, NULL, NULL, NULL, NULL, '2020-06-30', '2020-07-02', '2020-07-02'),
(4, 'Trumpicorn', 'Une licorne au pelage doré, décidée à construire un mur à la frontière de son pâturage', 10, 1.5, '#ffdd00', 802, 1, 'https://i5.walmartimages.com/asr/247941f1-fab1-4394-acac-aa25830e58a7_1.a918eb0876f075680c9bc02931b8e08e.jpeg', 3, 4.99, NULL, NULL, NULL, NULL, '2020-07-01', '2020-07-01', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `num_card` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_card` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cvc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expiration_month` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expiration_year` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `num_card`, `name_card`, `cvc`, `expiration_month`, `expiration_year`) VALUES
(1, 'Tomamy', 'dutilly.thomas@gmail.com', NULL, '$2y$10$yGv76/ev.Km6qgObeekkS.dwtS42QW6bHVn9RbDO5nKjZ.EPmQiDG', NULL, '2020-06-29 12:18:16', '2020-06-29 12:18:16', NULL, NULL, NULL, NULL, NULL),
(2, 'toma', 'thomas.dutilly@imie.fr', NULL, '$2y$10$sahMZQndHTnYPRNInUDhjO.5H6FmCz3yIuCGkT4g/LwCGkVpIlIL6', NULL, '2020-06-30 06:52:04', '2020-06-30 06:52:04', NULL, NULL, NULL, NULL, NULL),
(3, 'jaram', 'jeremy.housseau@imie.fr', NULL, '$2y$10$b3DtBvtp7ujer.hWG2tNLedcIq7L4JKAFyA7raYX46DCLasffeSFy', NULL, '2020-07-01 05:17:45', '2020-07-02 10:38:01', 'eyJpdiI6InJoSWxTNmdNaG5ieWV1OVZHTWtFVWc9PSIsInZhbHVlIjoiL0pvQ2lYZk1uc0NnYzJ3NG1Cb0R5NWo2emJ1Q2hSZEVnTW8wS3ZIWGhhQT0iLCJtYWMiOiJmOTI2NzhjZGIwYTkwNTRkMmQ3ODJlNWY2N2NiOGE0NzJiNWQwMTAzZDExOTk3ODVjNTQ5ODk5ZGZjYzBhZjk5In0=', 'eyJpdiI6Ik14ZWlmZjJQa25NL3V6ZGZ4T1A1UlE9PSIsInZhbHVlIjoiWEQ1bkc1Z0x2ZjNqaUNwU0hsTkNpdz09IiwibWFjIjoiMWY4MTNmYzA5ZjAwYWExNmRmMDI4Yjg5ZjhmMjJkYzU2ZGRlMmVhYjRlYzJmZWI0ZDIzMmFkODZlM2JlNGMzMyJ9', 'eyJpdiI6IllwbFA3bkxhNzAvTFhKQjZ1b0hhbHc9PSIsInZhbHVlIjoiMG5JdmtlMXIxNGNLNGs1OC9uQjAvdz09IiwibWFjIjoiODNhMjZlMmU0ZTQwYjRjMjQzYjJiZmY3OTMzYjgzMTBiNzgwNTViODM1YThhOGMwZDk4ZGVkYTZmNzMxMTdiYiJ9', 'eyJpdiI6InZ0ckF5eWFVWWIvc2E3SmJ0bVMvc0E9PSIsInZhbHVlIjoiRVFrK1BHQVFOL3hmbWZxTUM5WGk3UT09IiwibWFjIjoiNTJhNjg5YTRjNGJkODJkYmRmM2Y3MjQ3OWQ5MDQyYmY2ZWExZWY2YzgyMzYwZDExODVhNTYxNmY1OTdmYzk3NCJ9', 'eyJpdiI6ImdWMVNnYk9YT01jNmFoQkx1Mk8wd2c9PSIsInZhbHVlIjoiZk5lRk5kc1gvZ1VzZ2E3b0dRdHErdz09IiwibWFjIjoiNTM4NWU1NDFmMjc2MWZkMzA2OTIxZjQ0NjA0M2Y2ZjRmM2E0ZTgzNTI4OTM4N2FlYjAwMzdmOGY5MTA2OTI2OSJ9');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
