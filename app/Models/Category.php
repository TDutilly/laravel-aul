<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = "categories";

    protected $fillable = [
        "id", "name"
    ];

    public function unicorns()
    {
        return $this->hasMany(Unicorn::class);
    }
}
