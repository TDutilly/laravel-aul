<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Unicorn extends Model
{
    use SoftDeletes;

    protected $table = "unicorns";

    protected $fillable = [
        "id", "name", "horn_size", "size", "color", "age", "category_id", "picture", "prix", "user_id", "deleted_at"
    ];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
