<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\Unicorn;
use App\Models\Category;

class UnicornController extends Controller
{

    public function create($category_id)
    {
        if(Auth::user()){
          if($category_id == 1 && is_null(Auth::user()->num_card)){
            return redirect()->route('users.show', Auth::user()->id);
          }else{
            return view('unicorns.create', compact('category_id'));
          }
        }else{
          return redirect()->route('login');
        }
    }

    public function store(Request $request)
    {
        $unicorn = new Unicorn();
        $unicorn->name = $request->get('name');
        $unicorn->description = $request->get('description');
        $unicorn->size = $request->get('size');
        $unicorn->horn_size = $request->get('horn_size');
        $unicorn->age = $request->get('age');
        $unicorn->color = $request->get('color');
        $unicorn->picture = $request->get('picture');
        $unicorn->category_id = $request->get('category_id');
        $unicorn->user_id = Auth::user()->id;
        if($request->get('category_id')==1){
          $unicorn->prix = $request->get('prix');
        }else{
          $unicorn->repro1_debut = $request->get('repro1_debut');
          $unicorn->repro1_fin = $request->get('repro1_fin');
          if(!is_null($request->get('repro2_debut')) && !is_null($request->get('repro2_fin'))){
            $unicorn->repro2_debut = $request->get('repro2_debut');
            $unicorn->repro2_fin = $request->get('repro2_fin');
          }
        }
        $unicorn->save();

        return redirect()->route('unicorns.show', $unicorn->id);
    }

    public function show($id)
    {
        $unicorn = Unicorn::find($id);

        // dd($unicorn->category->name);

        return view('unicorns.show', compact('unicorn'));
    }

    public function index()
    {
        $unicorns = Unicorn::all();

        return view('unicorns.index', compact('unicorns'));
    }

    public function edit($id)
    {
        $unicorn = Unicorn::find($id);

        return view('unicorns.edit', compact('unicorn'));
    }

    public function update(Request $request, $id)
    {
        $unicorn = Unicorn::find($id);
        $unicorn->name = $request->get('name');
        $unicorn->description = $request->get('description');
        $unicorn->size = $request->get('size');
        $unicorn->horn_size = $request->get('horn_size');
        $unicorn->age = $request->get('age');
        $unicorn->color = $request->get('color');
        $unicorn->picture = $request->get('picture');
        $unicorn->user_id = Auth::user()->id;
        if($unicorn->category_id==1){
          $unicorn->prix = $request->get('prix');
        }else{
          $unicorn->repro1_debut = $request->get('repro1_debut');
          $unicorn->repro1_fin = $request->get('repro1_fin');
          if(!is_null($request->get('repro2_debut')) && !is_null($request->get('repro2_fin'))){
            $unicorn->repro2_debut = $request->get('repro2_debut');
            $unicorn->repro2_fin = $request->get('repro2_fin');
          }
        }
        $unicorn->save();
        return redirect()->route('unicorns.show', $unicorn->id);
    }

    public function destroy(Request $request)
    {
        $unicorn = Unicorn::find($request->get('id'));
        $unicorn->delete();

        return redirect()->route('unicorns.index');
    }
}
