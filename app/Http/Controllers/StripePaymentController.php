<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;
use Stripe;

use App\Models\Unicorn;

class StripePaymentController extends Controller
{
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function stripe($id)
    {
        if(Auth::user()){
          $unicorn = Unicorn::find($id);
          return view('stripe', compact('unicorn'));
        }
        return redirect()->route('login');
    }

    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function stripePost(Request $request, $id)
    {
        $unicorn = Unicorn::find($id);

        Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        Stripe\Charge::create ([
                "amount" => $unicorn->prix*100,
                "currency" => "usd",
                "source" => $request->stripeToken,
                "description" => "Achat de ".$unicorn->name."."
        ]);

        $unicorn->delete();

        Session::flash('success', 'Payment successful!');

        return redirect()->route('home');
    }
}
