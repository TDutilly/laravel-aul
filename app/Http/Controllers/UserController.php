<?php
//SQLSTATE[42S22]: Column not found: 1054 Champ 'unicorns.user_id' inconnu dans where clause (SQL: select * from `unicorns` where `unicorns`.`user_id` in (3))
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

use App\Models\User;
use App\Models\Unicorn;

class UserController extends Controller
{
  public function show($id)
  {
      $user = User::find($id);

      return view('users.show', compact('user'));
  }

  public function bank(Request $request, $id)
  {
      $user = User::find($id);

      $user->num_card = Crypt::encryptString($request->get('num_card'));
      $user->name_card = Crypt::encryptString($request->get('name_card'));
      $user->cvc = Crypt::encryptString($request->get('cvc'));
      $user->expiration_month = Crypt::encryptString($request->get('expiration_month'));
      $user->expiration_year = Crypt::encryptString($request->get('expiration_year'));
      $user->save();
      return redirect()->route('users.show', $user->id);
  }
}
