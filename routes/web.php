<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/categories', 'CategoryController@index')->name('categories.index');
Route::get('/categories/{id}/show', 'CategoryController@show')->name('categories.show');
Route::get('/categories/create', 'CategoryController@create')->name('categories.create');
Route::get('/categories/{id}/edit', 'CategoryController@edit')->name('categories.edit');
Route::post('/categories', 'CategoryController@store')->name('categories.store');
Route::put('/categories/{id}', 'CategoryController@update')->name('categories.update');
Route::delete('/categories', 'CategoryController@destroy')->name('categories.destroy');

Route::get('/unicorns', 'UnicornController@index')->name('unicorns.index');
Route::get('/unicorns/{id}/show', 'UnicornController@show')->name('unicorns.show');
Route::get('/unicorns/{category_id}/create', 'UnicornController@create')->name('unicorns.create');
Route::get('/unicorns/{id}/edit', 'UnicornController@edit')->name('unicorns.edit');
Route::post('/unicorns', 'UnicornController@store')->name('unicorns.store');
Route::put('/unicorns/{id}', 'UnicornController@update')->name('unicorns.update');
Route::delete('/unicorns', 'UnicornController@destroy')->name('unicorns.destroy');

Route::get('/users/{id}/show', 'UserController@show')->name('users.show');
Route::put('/users/{id}', 'UserController@bank')->name('users.bank');

Route::get('{id}/stripe', 'StripePaymentController@stripe')->name('stripe');
Route::post('{id}/stripe', 'StripePaymentController@stripePost')->name('stripe.post');
