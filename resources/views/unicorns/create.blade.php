@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Création d'un article</div>

                <div class="card-body">

                    <form action="{{ route('unicorns.store') }}" method="POST">
                        @csrf
                        <input type="hidden" class="form-control" name="category_id" value="{{ $category_id }}">
                        <div class="form-group">
                          <label for="name">Nom</label>
                          <input required id="name" class="form-control" type="text" name="name">
                        </div>
                        <div class="form-group">
                          <label for="description">Description</label>
                          <textarea required id="description" class="form-control" type="text" name="description"></textarea>
                        </div>
                        <div class="form-group">
                          <label for="size">Taille au garot</label>
                          <div class="input-group mb-3">
                            <input required id="size" class="form-control" type="text" name="size">
                            <div class="input-group-append">
                              <span class="input-group-text" id="basic-addon2">mètres</span>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="horn_size">Longueur de la corne</label>
                          <div class="input-group mb-3">
                            <input required id="horn_size" class="form-control" type="text" name="horn_size">
                            <div class="input-group-append">
                              <span class="input-group-text" id="basic-addon2">centimètres</span>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="age">Age</label>
                          <div class="input-group mb-3">
                            <input required id="age" class="form-control" type="number" name="age">
                            <div class="input-group-append">
                              <span class="input-group-text" id="basic-addon2">saisons</span>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="color">Couleur du pelage</label>
                          <input required id="color" class="form-control" type="color" name="color">
                        </div>
                        <div class="form-group">
                          <label for="picture">Lien de la photo</label>
                          <input required id="picture" class="form-control" type="text" name="picture">
                        </div>

                        @if($category_id == 1)
                        <div class="form-group">
                          <label for="prix">Prix</label>
                          <div class="input-group mb-3">
                            <input required id="prix" class="form-control" type="text" name="prix">
                            <div class="input-group-append">
                              <span class="input-group-text" id="basic-addon2">$</span>
                            </div>
                          </div>
                        </div>
                        @else
                        <div class="form-group">
                          <label for="repro1_debut">Période de reproduction 1</label>
                          <input required id="repro1_debut" class="form-control" type="date" name="repro1_debut">
                          <input required id="repro1_fin" class="form-control" type="date" name="repro1_fin">
                        </div>
                        <div class="form-group">
                          <label for="repro2_debut">Période de reproduction 1</label>
                          <input id="repro2_debut" class="form-control" type="date" name="repro2_debut">
                          <input id="repro2_fin" class="form-control" type="date" name="repro2_fin">
                        </div>
                        @endif

                        <br>
                        <a href="{{ route('home') }}" class="btn btn-danger" title="Annuler">Annuler</a>
                        <button class="btn btn-primary" type="submit">Envoyer</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
