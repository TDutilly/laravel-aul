@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Fiche de la licorne</div>

                <div class="card-body">

                  <div class="card">
                    <div class="card-body">
                      <img src="{{ $unicorn->picture }}" alt="{{ $unicorn->picture }}" style="float:left; max-height:150px; padding-right:20px;">
                      <h5 class="card-title">{{ $unicorn->name }}</h5>
                      <p class="card-text">{{ $unicorn->description }}</p>
                    </div>
                    <ul class="list-group list-group-flush">
                      <li class="list-group-item">Type : {{ $unicorn->category->name }}</li>
                      <li class="list-group-item">Propriétaire : {{ $unicorn->owner->name }}</li>
                      <li class="list-group-item">Taille : {{ $unicorn->size }}m | Corne : {{ $unicorn->horn_size }}cm</li>
                      <li class="list-group-item">Age: {{ $unicorn->age }} saisons | Pelage : <span class="badge badge-pill" style="background:{{ $unicorn->color }}">&nbsp;</span></li>
                    </ul>
                    <div class="card-body">
                      @if($unicorn->category_id == 2)
                        <label>Période(s) de reproduction :</label>
                        @if(!is_null($unicorn->repro1_debut) && !is_null($unicorn->repro1_fin))
                          <li>{{ date("d/m",strtotime($unicorn->repro1_debut)) }} - {{ date("d/m",strtotime($unicorn->repro1_fin)) }}</li>
                        @endif
                        @if(!is_null($unicorn->repro2_debut) && !is_null($unicorn->repro2_fin))
                          <li>{{ date("d/m",strtotime($unicorn->repro2_debut)) }} - {{ date("d/m",strtotime($unicorn->repro2_fin)) }}</li>
                        @endif
                      @else
                        <label>Prix :</label>
                        <li>{{ $unicorn->prix }} $</li>
                      @endif
                    </div>
                    <div class="card-body clearfix">
                      @if(!is_null(Auth::user()) && $unicorn->user_id == Auth::user()->id)
                        <a class="btn btn-primary float-left" href="{{ route('unicorns.edit', $unicorn->id) }}">Modifier</a>
                        <form class="float-right" style="display:inline-block" action="{{ route('unicorns.destroy') }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <input type="hidden" name="id" value="{{ $unicorn->id }}">
                            <button class="btn btn-danger" type="submit">Supprimer</button>
                        </form>
                      @else
                        @if($unicorn->category_id == 2)
                          <a class="btn btn-primary text-white float-left" href="{{ route('users.show', $unicorn->user_id) }}">Contacter le propriétaire</a>
                        @else
                          <a class="btn btn-success float-left" href="{{ route('stripe', $unicorn->id) }}">J'achète</a>
                        @endif
                      @endif
                    </div>
                  </div>
                  <br>
                  <a class="btn btn-light" href="{{ route('unicorns.index') }}">Retour au magasin</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
