@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Licornes disponibles</div>

                <div class="card-body">

                  @foreach($unicorns as $unicorn)
                    <div class="col-sm-12">
                      <div class="card">
                        <div class="card-body">
                          <img class="unicorn-picture-list" src="{{ $unicorn->picture }}" alt="{{ $unicorn->picture }}" style="float:left; max-height:150px; padding-right:20px;">
                          <h5 class="card-title">
                            {{ $unicorn->name }}
                            <span class="badge badge-pill" style="background:{{ $unicorn->color }}">&nbsp;</span>
                          </h5>
                          <p class="card-text">
                            {{ $unicorn->category->name }}
                            @if($unicorn->category_id == 1)
                               - {{ $unicorn->prix }} $
                            @endif
                          </p>
                          <p class="card-text">{{ $unicorn->description }}</p>
                          <a href="{{ route('unicorns.show', $unicorn->id) }}" class="btn btn-primary" title="Voir la fiche">Voir la fiche</a>
                        </div>
                      </div>
                    </div>
                  @endforeach

                  <br>
                  <a href="{{ route('home') }}" class="btn btn-secondary" title="Retour sur la page d'accueil">Retour sur la page d'accueil</a>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
