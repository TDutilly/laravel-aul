@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Compte de {{ $user->name }}</div>

                <div class="card-body">
                    <div class="col-sm-12">
                      <h5 class="card-title">
                        @if(!is_null(Auth::user()) && Auth::user()->id == $user->id)
                          <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#coordonneesBancaires">Coordonnées bancaires</button>
                        @endif
                        Utilisateur : {{ $user->name }}<br>
                        Email : {{ $user->email}}
                      </h5>
                    </div>
                    <br>
                    <h4>Licornes possédées</h4>
                    @foreach($user->unicorns as $unicorn)
                      <div class="col-sm-12">
                        <div class="card">
                          <div class="card-body">
                            <img class="unicorn-picture-list" src="{{ $unicorn->picture }}" alt="{{ $unicorn->picture }}" style="float:left; max-height:150px; padding-right:20px;">
                            <h5 class="card-title">
                              {{ $unicorn->name }}
                              <span class="badge badge-pill" style="background:{{ $unicorn->color }}">&nbsp;</span>
                            </h5>
                            <p class="card-text">
                              {{ $unicorn->category->name }}
                              @if($unicorn->category_id == 1)
                                 - {{ $unicorn->prix }} $
                              @endif
                            </p>
                            <p class="card-text">{{ $unicorn->description }}</p>
                            <a href="{{ route('unicorns.show', $unicorn->id) }}" class="btn btn-primary" title="Voir la fiche">Voir la fiche</a>
                            @if(!is_null(Auth::user()) && $unicorn->user_id == Auth::user()->id)
                              <a href="{{ route('unicorns.edit', $unicorn->id) }}" class="btn btn-secondary" title="Modifier la fiche">Modifier la fiche</a>
                            @endif
                          </div>
                        </div>
                      </div>
                    @endforeach

                  <br>
                  <a href="{{ route('home') }}" class="btn btn-secondary" title="Retour sur la page d'accueil">Retour sur la page d'accueil</a>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal de coordonnées bancaires -->
<div class="modal fade" id="coordonneesBancaires" tabindex="-1" role="dialog" aria-labelledby="coordonneesBancairesLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="coordonneesBancairesLabel">Modification de vos coordonnées bancaires</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form
        action="{{ route('users.bank', $user->id) }}"
        method="POST"
        class="require-validation"
        id="payment-form">
        @csrf
        @method('PUT')
        <div class="modal-body">
            <div class='form-row row'>
                <div class='col-12 form-group required'>
                    <label class='control-label'>Nom du titulaire de la carte</label>
                    <input required name="name_card" class='form-control' type='text' value="{{ $user->name_card ? Crypt::decryptString($user->name_card) : "" }}">
                </div>
            </div>

            <div class='form-row row'>
                <div class='col-12 form-group required'>
                    <label class='control-label'>Numéro de la carte</label>
                    <input required name="num_card" autocomplete='off' class='form-control card-number' type='text' value="{{ $user->num_card ? Crypt::decryptString($user->num_card) : "" }}">
                </div>
            </div>
            <div class='form-row row'>
                <div class='col-xs-12 col-md-4 form-group cvc required'>
                    <label class='control-label'>CVC</label>
                    <input required name="cvc" autocomplete='off' class='form-control card-cvc' placeholder='ex. 311' size='4' type='text' value="{{ $user->cvc ? Crypt::decryptString($user->cvc) : "" }}">
                </div>
                <div class='col-xs-12 col-md-4 form-group expiration required'>
                    <label class='control-label'>Mois d'expiration</label>
                    <input required name="expiration_month" class='form-control card-expiry-month' placeholder='MM' size='2' type='text' value="{{ $user->expiration_month ? Crypt::decryptString($user->expiration_month) : "" }}">
                </div>
                <div class='col-xs-12 col-md-4 form-group expiration required'>
                    <label class='control-label'>Année d'expiration</label>
                    <input required name="expiration_year" class='form-control card-expiry-year' placeholder='YYYY' size='4' type='text' value="{{ $user->expiration_year ? Crypt::decryptString($user->expiration_year) : "" }}">
                </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button class="btn btn-primary" type="submit">Valider</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
