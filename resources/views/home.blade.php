@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Accueil</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <a href="{{ route('unicorns.index') }}" class="btn btn-success" title="Accéder au magasin">Accéder au magasin</a>
                    <a href="{{ route('unicorns.create', 1) }}" class="btn btn-success" title="Vendez votre licorne">Vendez votre licorne</a>
                    <a href="{{ route('unicorns.create', 2) }}" class="btn btn-success" title="Proposez une licorne reproductrice">Proposez une licorne reproductrice</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
